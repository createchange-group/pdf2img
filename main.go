package main

import (
	"fmt"
	"image/jpeg"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/karmdip-mi/go-fitz"
)

func main() {

	var files []string

	args := os.Args

	root := args[1] //+ "pdf/"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == ".pdf" {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		doc, err := fitz.New(file)
		if err != nil {
			panic(err)
		}
		folder := strings.TrimSuffix(path.Base(file), filepath.Ext(path.Base(file)))

		// Extract pages as images
		for n := 0; n < doc.NumPage(); n++ {
			img, err := doc.Image(n)
			if err != nil {
				panic(err)
			}
			homedir, err := os.UserHomeDir()
			if err != nil {
				panic(err)
			}

			err = os.MkdirAll(homedir+"/Pictures/pdf2img/"+folder, 0755)
			if err != nil {
				panic(err)
			}

			f, err := os.Create(filepath.Join(homedir+"/Pictures/pdf2img/"+folder+"/", fmt.Sprintf("image-%05d.jpg", n)))
			if err != nil {
				panic(err)
			}

			err = jpeg.Encode(f, img, &jpeg.Options{Quality: jpeg.DefaultQuality})
			if err != nil {
				panic(err)
			}

			f.Close()

		}
	}
}
